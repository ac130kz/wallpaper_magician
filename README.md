# Wallpaper Magician app

## How to get prepared

- Install Flutter SDK `https://flutter.dev/docs/get-started/install` and add to PATH
- For Android, Install Android Studio and Android SDK with platform;android-31, the build system may require other components and will request them when the compilation fails.
- For iOS, the latest XCode is preferred, update Ruby dependencies as shown in the "Getting Started".

## How to run debug version on the emulator (Android)/simulator (iOS):

```bash
flutter run
```

## Building release for Android

Once the keys are generated and stored properly:

```bash
flutter build appbundle --release --obfuscate --tree-shake-icons --split-debug-info=.debug_info
```

For more info: `https://flutter.dev/docs/deployment/android`

## Building release for iOS

Once the keys are generated and store properly:

```bash
flutter build ios --release --obfuscate --tree-shake-icons --split-debug-info=.debug_info
```

For more info: `https://flutter.dev/docs/deployment/ios`

Ruby dependencies may require a VPN to download since it's blocked in Kazakhstan.

## MISC: Flutter config

```bash
flutter config --no-enable-windows-desktop
flutter config --no-enable-web
flutter config --no-enable-macos-desktop
flutter config --no-enable-linux-desktop
flutter config --no-enable-fuchsia
flutter config --no-enable-windows-uwp-desktop
flutter config --no-analytics
```
