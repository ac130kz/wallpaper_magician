class ImageSource {
  final String name;
  final String url;

  const ImageSource({
    required this.name,
    required this.url,
  });
}
