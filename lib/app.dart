import "dart:io";
import "dart:async";
import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:uuid/uuid.dart";
import "package:async_wallpaper/async_wallpaper.dart";
import "package:permission_handler/permission_handler.dart";
import "package:dio/dio.dart";
import "package:adaptive_theme/adaptive_theme.dart";
import "package:path_provider/path_provider.dart";
import "package:wallpaper_magician/image_source.dart";
import "package:wallpaper_magician/const.dart";

class App extends StatefulWidget {
  final AdaptiveThemeMode savedTheme;

  const App({
    Key? key,
    required this.savedTheme,
  }) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  late final _lightColorScheme =
      ColorScheme.fromSwatch(primarySwatch: Colors.purple)
          .copyWith(secondary: Colors.blue, brightness: Brightness.light);
  late final _darkColorScheme =
      _lightColorScheme.copyWith(brightness: Brightness.dark);
  String _wallpaperStatus = "App initialized";
  static const uuid = Uuid();
  final dio = Dio();
  ImageSource _source = imageSources[0];
  File? file;

  String getFileExtension(HttpClientResponse response) {
    var fileExtension = "";
    final contentTypeHeader = response.headers[HttpHeaders.contentTypeHeader];
    if (contentTypeHeader != null) {
      final contentType = ContentType.parse(contentTypeHeader.join());
      fileExtension = contentType.subType;
    }
    return fileExtension;
  }

  Future<void> _setWallpaperFromFile(File file) async {
    String result;
    try {
      final status = await AsyncWallpaper.setWallpaperFromFile(
        filePath: file.absolute.path,
      );
      result = status ? "Success" : "Failure";
    } catch (e) {
      if (e is PlatformException || e is FileSystemException || e is OSError) {
        result = "Failed to get the wallpaper.";
      } else {
        rethrow;
      }
    }

    if (!mounted) return;

    setState(() {
      _wallpaperStatus = result;
    });
  }

  Future<void> getWallpaper() async {
    if (!await Permission.storage.request().isGranted) {
      setState(() {
        _wallpaperStatus = "Storage permission denied";
      });
      return;
    }

    setState(() {
      _wallpaperStatus = "Loading";
    });

    final client = HttpClient();
    client.getUrl(Uri.parse(_source.url)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) async {
      final fileName = uuid.v4();
      final tempDir = await getTemporaryDirectory();
      final fileExtension = getFileExtension(response);
      if (file != null) {
        await file!.delete();
        setState(() {
          file = null;
        });
      }
      file = File("${tempDir.path}/$fileName.$fileExtension");
      await response.pipe(file!.openWrite());
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
      light: ThemeData(
        brightness: Brightness.light,
        colorScheme: _lightColorScheme,
      ),
      dark: ThemeData(
        brightness: Brightness.dark,
        colorScheme: _darkColorScheme,
      ),
      initial: widget.savedTheme,
      builder: (theme, darkTheme) => MaterialApp(
        title: "Wallpaper Magician",
        theme: theme,
        darkTheme: darkTheme,
        home: Scaffold(
          appBar: AppBar(
            title: const Text("Wallpaper Magician"),
            actions: [
              IconButton(
                icon: Icon(
                  widget.savedTheme.isLight
                      ? Icons.light_mode
                      : Icons.dark_mode,
                ),
                // TODO: unable to find AdaptiveTheme ancestor
                onPressed: () => AdaptiveTheme.of(context).toggleThemeMode,
              )
            ],
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // TODO: preview
              // if (file != null)
              //   Image(
              //     width: MediaQuery.of(context).size.height / 2,
              //     height: MediaQuery.of(context).size.height / 3,
              //     filterQuality: FilterQuality.high,
              //     image: FileImage(
              //       file!,
              //     ),
              //   ),
              DropdownButton<ImageSource>(
                value: _source,
                icon: const Icon(Icons.arrow_downward, size: 12),
                elevation: 16,
                onChanged: (ImageSource? newValue) {
                  setState(() {
                    _source = newValue!;
                  });
                },
                items: imageSources
                    .map<DropdownMenuItem<ImageSource>>((ImageSource source) {
                  return DropdownMenuItem<ImageSource>(
                    value: source,
                    child: Text(source.name),
                  );
                }).toList(),
              ),
              const SizedBox(height: 32),
              ElevatedButton(
                onPressed: getWallpaper,
                child: _wallpaperStatus == "Loading"
                    ? const SizedBox(
                        height: 12,
                        width: 12,
                        child: CircularProgressIndicator(color: Colors.white),
                      )
                    : const Text("Get wallpaper!"),
              ),
              const SizedBox(height: 32),
              if (file != null)
                ElevatedButton(
                  onPressed: () => _setWallpaperFromFile(file!),
                  child: const Text("Set wallpaper!"),
                ),
              Center(child: Text("Wallpaper status: $_wallpaperStatus\n")),
            ],
          ),
        ),
      ),
    );
  }
}
