import "package:wallpaper_magician/image_source.dart";

const imageSources = <ImageSource>[
  ImageSource(
    name: "Unsplash",
    url:
        "https://source.unsplash.com/2160x3840/?nature,featured,act-for-nature,wallpapers,3d-renders,textures-patterns,experimental,architecture,street-photography,travel,animals,arts-culture",
  ),
  ImageSource(
    name: "Wikipedia",
    url: "https://commons.wikimedia.org/",
    // TODO: https://github.com/Rudloff/muzei-commons/blob/master/src/main/java/pro/rudloff/muzei/commons/CommonsService.kt
  ),
  ImageSource(
    name: "Bing",
    url: "2160x3840/",
    // TODO: https://github.com/devmil/muzei-bingimageoftheday/commit/d094d754a39fc0f53e667ada20794327f530b5c5
    // https://github.com/devmil/muzei-bingimageoftheday/blob/d094d754a39fc0f53e667ada20794327f530b5c5/app/src/main/java/de/devmil/muzei/bingimageoftheday/worker/BingImageOfTheDayWorker.kt
  )
];
