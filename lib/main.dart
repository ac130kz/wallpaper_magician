import "dart:async";
import "package:flutter/material.dart";
import "package:adaptive_theme/adaptive_theme.dart";
import "package:wallpaper_magician/app.dart";

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final savedThemeMode = await AdaptiveTheme.getThemeMode();
  runApp(App(savedTheme: savedThemeMode ?? AdaptiveThemeMode.dark));
}
